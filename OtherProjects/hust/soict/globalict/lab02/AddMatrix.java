package OtherProjects.hust.soict.globalict.lab02;

import java.util.Scanner;
import java.util.Arrays;
public class AddMatrix {
    protected int rows = 0, cols = 0;
    static Scanner sc;
    int [][] matA, matB, matC;

    AddMatrix(int numRows, int numCols){
        this.rows = numRows;
        this.cols = numCols;
        matA = new int[rows][cols];
        matB = new int[rows][cols];
        matC = new int[rows][cols];
    }

    void setMatrix(int [][] mat){
        for(int i=0;i<this.rows;++i){
            for(int j=0;j<this.cols;++j){
                System.out.print("Element row " + (i+1) + ", column " + (j+1) + ": ");
                mat[i][j] = sc.nextInt();
            }
        }
    }

    void addMatrix(){
        for(int i=0;i<this.rows;++i){
            for(int j=0;j<this.cols;++j){
                this.matC[i][j] = this.matA[i][j] + this.matB[i][j];
            }
        }
    }

    void displayFirstMatrix(){
        System.out.println("First matrix:");
        for(int i=0;i<this.rows;++i){
            System.out.println(Arrays.toString(this.matA[i]));
        }
    }

    void displaySecondMatrix(){
        System.out.println("Second matrix:");
        for(int i=0;i<this.rows;++i){
            System.out.println(Arrays.toString(this.matB[i]));
        }
    }

    void displayResult(){
        System.out.println("Sum of two entered matrices:");
        for(int i=0;i<this.rows;++i){
            System.out.println(Arrays.toString(this.matC[i]));
        }
    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int numRows = 0, numCols = 0;
        System.out.print("Enter number of rows of two matrices: ");
        numRows = sc.nextInt();
        System.out.print("Enter number of columns of two matrices: ");
        numCols = sc.nextInt();
        AddMatrix am = new AddMatrix(numRows,numCols);
        System.out.println("Enter the first matrix.");
        am.setMatrix(am.matA);
        System.out.println("Enter the second matrix.");
        am.setMatrix(am.matB);
        am.addMatrix();
        am.displayResult();
    }
}
