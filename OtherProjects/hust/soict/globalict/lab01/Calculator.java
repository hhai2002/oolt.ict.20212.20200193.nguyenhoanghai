package OtherProjects.hust.soict.globalict.lab01;

import java.util.Scanner;

public class Calculator {
    Scanner sc;

    Calculator(){
        sc = new Scanner(System.in);
    }

    double readInput(){
        return sc.nextDouble();
    }

    double sum(double a, double b){
        return a + b;
    }

    double difference(double a, double b){
        return a - b;
    }

    double product(double a, double b){
        return a * b;
    }

    double quotient(double a, double b){
        return a / b;
    }

    void showResult(double a, double b){
        System.out.println("Sum: " + sum(a,b));
        System.out.println("Difference: " + difference(a,b));
        System.out.println("Product: " + product(a,b));
        if(b!=0.0){
            System.out.println("Quotient: " + quotient(a,b));  
        }      
    }

    public static void main(String[] args) {
        Calculator cal = new Calculator();
        System.out.println("Enter first double number: ");
        double num1 = cal.readInput();
        System.out.println("Enter second double number: ");
        double num2 = cal.readInput();
        System.out.println("You entered: " + num1 + "  " + num2);
        cal.showResult(num1, num2);
    }
}
