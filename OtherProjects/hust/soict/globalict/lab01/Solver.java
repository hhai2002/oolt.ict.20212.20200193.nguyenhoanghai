package OtherProjects.hust.soict.globalict.lab01;

import java.util.Scanner;

public class Solver {
    /* Solve first-degree equation, one variable with a form a * x + b = 0
        where a and b are coefficients, 
        x is variable.
    */

    Scanner sc;

    Solver(){
        sc = new Scanner(System.in);
    }
    
    void fdovSolver(double a, double b){
        System.out.println("Solve " + a + "x + " + b + " = 0");
        if(a != 0.0){
            double x = - b / a;
            System.out.println("Unique solution: " + x);
        }
        else{
            if(b == 0.0){
                System.out.println("Infinite solutions.");
            }
            else{
                System.out.println("No solution.");
            }
        }
    }

    /* Calculate determinant of matrix with the form
        | a  b |
        | c  d |
    */

    double determinant(double a, double b, double c, double d){
        return a * d - b * c;
    }


    /* Solve system of first-degree equation, two variables with a form
        a * x1 + b * x2 = c
        d * x1 + e * x2 = f
        where a, b, c, d, e, f are coefficients,
        x1 and x2 are variables.
    */

    void sfdtwSolver(double a, double b, double c, double d, double e, double f){
        System.out.println("Solve ");
        System.out.println("\t" + a + " * x1 + " + b + " * x2 = " + c);
        System.out.println("\t" + d + " * x1 + " + e + " * x2 = " + f);
        double D = determinant(a, b, d, e);
        double D1 = determinant(c, b, f, e);
        double D2 = determinant(a, c, d, f);
        if(D != 0.0){
            double x1 = D1 / D;
            double x2 = D2 / D;
            System.out.println("Unique solution: x1 = " + x1 + "\tx2 = " + x2);
        }
        else{
            if(D1 == 0.0 && D2 == 0.0){
                System.out.println("Infinite solutions");
            }
            else{
                System.out.println("No solution.");
            }
        }
    }

    /* Solve second-degree equation, one variable with the form
        a * x ^ 2 + b * x + c = 0
        where a, b, c are coefficients,
        x is variable
    */

    void sdovSolver(double a, double b, double c){
        if(a == 0.0){
            fdovSolver(b, c);
        }
        else{
            System.out.println("Solve " + a + " * x ^ 2 + " + b + " * x + " + c + " = 0");
            double delta = b * b - 4.0 * a * c;
            if(delta == 0.0){
                double x = - b / (2.0 * a);
                System.out.println("Double root: x = " + x);
            }
            else if(delta > 0.0){
                double x1 = (- b + Math.sqrt(delta)) / (2 * a);
                double x2 = (- b - Math.sqrt(delta)) / (2 * a);
                System.out.println("Distinct roots: x1 = " + x1 + "\tx2 = " + x2);
            }
            else{
                System.out.println("No solution.");
            }
        }
    }

    void menu(){
        System.out.println("Choose a type of equation to start: ");
        System.out.println("1.\t First-degree equation, one variable");
        System.out.println("2.\t System of first-degree equation, two variables");
        System.out.println("3.\t Second-degree equation, one variable");
        System.out.println("0.\t Exit");
    }

    void firstDegreeOneVariable(){
        System.out.println("Solve first-degree equation, one variable with a form a * x + b = 0");
        System.out.println("Now please enter coefficients a, b.");
        double a = 0.0;
        double b = 0.0;
        System.out.print("a = ");
        a = sc.nextDouble();
        System.out.print("b = ");
        b = sc.nextDouble();
        fdovSolver(a, b);
    }

    void firstDegreeTwoVariables(){
        System.out.println("Solve system of first-degree equation, two variables with a form");
        System.out.println("\ta * x1 + b * x2 = c");
        System.out.println("\td * x1 + e * x2 = f");
        System.out.println("Now please enter coefficients a, b, c, d, e, f.");
        double a = 0.0, b = 0.0, c = 0.0, d = 0.0, e = 0.0, f = 0.0;
        System.out.print("a = ");
        a = sc.nextDouble();
        System.out.print("b = ");
        b = sc.nextDouble();
        System.out.print("c = ");
        c = sc.nextDouble();
        System.out.print("d = ");
        d = sc.nextDouble();
        System.out.print("e = ");
        e = sc.nextDouble();
        System.out.print("f = ");
        f = sc.nextDouble();
        sfdtwSolver(a, b, c, d, e, f);
    }

    void secondDegreeOneVariable(){
        System.out.println("Solve second-degree equation, one variable with the form");
        System.out.println("\ta * x ^ 2 + b * x + c = 0");
        System.out.println("Now please enter coefficients a, b, c.");
        double a = 0.0, b = 0.0, c = 0.0;
        System.out.print("a = ");
        a = sc.nextDouble();
        System.out.print("b = ");
        b = sc.nextDouble();
        System.out.print("c = ");
        c = sc.nextDouble();
        sdovSolver(a, b, c);
    }
    

    public static void main(String[] args) {
        Solver s = new Solver();
        int choice = 0;
        do{
            s.menu();
            choice = s.sc.nextInt();
            switch(choice){
                case 0:{
                    break;
                }
                case 1:{
                    s.firstDegreeOneVariable();
                    break;
                } 
                case 2:{
                    s.firstDegreeTwoVariables();
                    break;
                }
                case 3:{
                    s.secondDegreeOneVariable();
                    break;
                }
                default:{
                    System.out.println("Invalid choice, please try again.");
                    break;
                }
            }
        }
        while(choice!=0);        
    }
}
