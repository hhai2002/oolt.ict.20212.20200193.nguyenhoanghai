package Aims.hust.soict.globalict.test.disc.TestPassingParameter;

import Aims.hust.soict.globalict.aims.media.DigitalVideoDisc;

class DigitalVideoDiscWrapper{
    DigitalVideoDisc disc;
    DigitalVideoDiscWrapper(DigitalVideoDisc disc){
        this.disc = disc;
    }
}

public class TestPassingParameter {
    public static void main(String[] args) {
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");

        DigitalVideoDiscWrapper d1 = new DigitalVideoDiscWrapper(jungleDVD);
        DigitalVideoDiscWrapper d2 = new DigitalVideoDiscWrapper(cinderellaDVD);

        swap(d1, d2);

        jungleDVD = d1.disc;
        cinderellaDVD = d2.disc;

        System.out.println("jungle dvd title: " + jungleDVD.getTitle());
        System.out.println("cinderella dvd title: " + cinderellaDVD.getTitle());

        changeTitle(jungleDVD, cinderellaDVD.getTitle());
        System.out.println("jungle dvd title: " + jungleDVD.getTitle());
    }
/*
    public static void swap(Object o1, Object o2){
        Object tmp = o1;
        o1 = o2;
        o2 = tmp;
    }

 */

    public static void swap(Object o1, Object o2){
        if(o1 instanceof DigitalVideoDiscWrapper & o2 instanceof DigitalVideoDiscWrapper){
            DigitalVideoDisc tmp = ((DigitalVideoDiscWrapper) o1).disc;
            ((DigitalVideoDiscWrapper) o1).disc = ((DigitalVideoDiscWrapper) o2).disc;
            ((DigitalVideoDiscWrapper) o2).disc = tmp;
        }
    }

    public static void changeTitle(DigitalVideoDisc dvd, String title){
        String oldTitle = dvd.getTitle();
        dvd.setTitle(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }
}
