package Aims.hust.soict.globalict.test.utils.DateTest;

import Aims.hust.soict.globalict.aims.utils.DateUtils;
import Aims.hust.soict.globalict.aims.utils.MyDate;

public class DateTest {
    public static void main(String[] args) {
        MyDate d1 = new MyDate("June 6th 2013");
        MyDate d2 = new MyDate(18,9,2017);
        MyDate d3 = new MyDate();
        MyDate d4 = new MyDate("October 10th 2002");
        MyDate[] d = new MyDate[4];

        d[0] = d1;
        d[1] = d2;
        d[2] = d3;
        d[3] = d4;

        d1.print();
        d2.print(2);
        d3.print();
        d4.print(3);

        DateUtils.sort(d);

        for (MyDate k : d) {
            k.print(4);
            System.out.print('\n');
        }
    }
}
