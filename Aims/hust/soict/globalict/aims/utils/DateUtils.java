package Aims.hust.soict.globalict.aims.utils;

import java.util.Arrays;

public class DateUtils{

    public static int compare(MyDate d1, MyDate d2){
        if(d1.getYear() < d2.getYear()) return -1;
        if(d1.getYear() > d2.getYear()) return 1;
        if(d1.getMonth() < d2.getMonth())   return -1;
        if(d1.getMonth() > d2.getMonth())   return 1;
        return Integer.compare(d1.getDay(), d2.getDay());
    }

    public static void sort(MyDate[] d){
        Arrays.sort(d, DateUtils::compare);
    }

}
