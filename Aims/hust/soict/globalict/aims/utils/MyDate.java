package Aims.hust.soict.globalict.aims.utils;

import java.time.LocalDate;
import java.util.Scanner;

public class MyDate {
    String[] monthList = {"January","February","March","April",
            "May","June","July","August",
            "September","October","November","December"};
    int day = 0;
    int month = 0;
    int year = 0;

    boolean isInteger(String str){
        try{
            Integer.parseInt(str);
            return true;
        }
        catch(Exception exc){
            return false;
        }
    }

    int[] conversion(String date){
        int[] day = new int[3];
        String[] d = date.split(" ");
        for(int i=0;i<this.monthList.length;++i){
            if(d[0].equals(this.monthList[i])){
                day[0] = i + 1;
                break;
            }
        }
        String cmp = d[1].substring(0,2);
        if (!isInteger(cmp)) {
            cmp = d[1].substring(0, 1);
        }
        day[1] = Integer.parseInt(cmp);
        day[2] = Integer.parseInt(d[2]);
        return day;
    }

    int countDays(int month, int year) {
        int count = 0;
        switch (month) {
            case 1, 3, 5, 7, 8, 10, 12 -> count = 31;
            case 4, 6, 9, 11 -> count = 30;
            case 2 -> {
                if (year % 4 == 0) {
                    count = 29;
                } else {
                    count = 28;
                }
                if ((year % 100 == 0) & (year % 400 != 0)) {
                    count = 28;
                }
            }
        }
        return count;
    }

    boolean isValidDate(int day, int month, int year){
        return (year > 0) && (month > 0) && (month < 13) && (day > 0) && (day < countDays(month,year));
    }

    public MyDate(){
        LocalDate ld = LocalDate.now();
        this.day = ld.getDayOfMonth();
        this.month = ld.getMonthValue();
        this.year = ld.getYear();
    }

    public MyDate(int day, int month, int year){
        if(isValidDate(day,month,year)) {
            this.day = day;
            this.month = month;
            this.year = year;
        }
    }

    public MyDate(String date){
        int[] day = conversion(date);
        if(isValidDate(day[1],day[0],day[2])) {
            this.month = day[0];
            this.day = day[1];
            this.year = day[2];
        }
    }


    public MyDate(String day, String month, String year){
        String[] dayStrings = {"first", "second", "third", "fourth", "fifth", "sixth",
                "seventh", "eighth", "ninth", "tenth", "eleventh",
                "twelfth", "thirteenth", "fourteenth", "fifteenth",
                "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth"
        };
        if(day.equals("thirtieth")) this.day = 30;
        if(day.equals("thirty-first"))this.day = 31;
        for(int i = 0; i < dayStrings.length; i++) {
            if(day.equals(dayStrings[i])) {
                this.day = i + 1;
                break;
            }
            if(day.equals("twenty " + dayStrings[i]) || day.equals("twenty-" + dayStrings[i])) {
                this.day = i + 21;
                break;
            }
        }
        for(int i=0;i<this.monthList.length;++i){
            if(month.equals(this.monthList[i])){
                this.month = i + 1;
            }
        }
        String[] yearStrings = {"one", "two", "three", "four", "five",
                "six", "seven", "eight", "nine", "ten",
                "eleven", "twelve", "thirteen", "fourteen", "fifteen",
                "sixteen", "seventeen", "eighteen", "nineteen", "twenty"
        };
        String[] yearStrings2 = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
        String[] strs = year.split(" ");
        if(strs.length == 2 || strs.length == 3) {
            for(int i = 0; i < yearStrings.length; i++) {
                if(strs[0].equals(yearStrings[i])) {
                    this.year = (i + 1)*100;
                    break;
                }
            }
            if(strs[1].equals("thousand")) {
                this.year *= 10;
            }
            else if(!strs[1].equals("hundred")) {
                for(int i = 0; i < yearStrings.length; i++) {
                    if(strs[strs.length-1].equals(yearStrings[i])) {
                        this.year += i + 1;
                        break;
                    }
                }
                outer:
                for(int i = 0; i < yearStrings2.length; i++) {
                    if(strs[1].equals(yearStrings2[i])) {
                        this.year += (i + 2)*10;
                        break;
                    }else {
                        for(int j = 0; j < 9; j++) {
                            if(strs[1].equals(yearStrings2[i] + "-" + yearStrings[j])) {
                                this.year += (i + 2)*10 + j + 1;
                                break outer;
                            }
                        }
                    }
                }
            }
        }
        if(strs.length == 4) {
            for(int i = 0; i < 10; i++) {
                if(strs[3].equals(yearStrings[i])) {
                    this.year = 2001 + i;
                }
            }
        }
    }


    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String convertOrdinal(int i) {
        String[] suffixes = new String[]{"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
        return switch (i % 100) {
            case 11, 12, 13 -> i + "th";
            default -> i + suffixes[i % 10];
        };
    }

    public void print(){
        System.out.println(this.monthList[this.getMonth()-1]+ " " + this.convertOrdinal(this.getDay()) + " " + this.getYear());
    }

    public void print(int option){
        switch (option){
            case 0->{
                this.print();
            }
            case 1->{
                String str = String.format("%04d", this.getYear());
                System.out.print(str + " - ");
                str = String.format("%02d", this.getMonth());
                System.out.print(str + " - ");
                str = String.format("%02d", this.getDay());
                System.out.println(str);
            }
            case 2->{
                System.out.println(this.getDay() + "/" + this.getMonth() + "/" + this.getYear());
            }
            case 3->{
                String str = String.format("%02d", this.getDay());
                System.out.print(str + "-");
                str = this.monthList[this.getMonth()-1].substring(0,3);
                System.out.print(str + "-");
                str = String.format("%04d", this.getYear());
                System.out.println(str);
            }
            case 4->{
                String str = this.monthList[this.getMonth()-1].substring(0,3);
                System.out.print(str + " " + this.getDay() + " " + this.getYear());
            }
            case 5->{
                String str = String.format("%02d", this.getMonth());
                System.out.print(str + " - ");
                str = String.format("%02d", this.getDay());
                System.out.print(str + " - ");
                str = String.format("%04d", this.getYear());
                System.out.println(str);
            }
        }
    }

    public void accept(){
        String date;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter date: ");
        date = sc.nextLine();
        int[] day = conversion(date);
        if(isValidDate(day[1],day[0],day[2])) {
            this.setMonth(day[0]);
            this.setDay(day[1]);
            this.setYear(day[2]);
        }
        else{
            System.out.println("Invalid date.");
        }
    }

    public static void main(String[] args) {
        MyDate md = new MyDate("February 7th 2019");
        md.print(5);
        md.accept();
        md.print();
    }
}
