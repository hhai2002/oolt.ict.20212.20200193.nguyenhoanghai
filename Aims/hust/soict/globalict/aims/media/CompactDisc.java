package Aims.hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable{
    private String artist;
    private ArrayList<Track> tracks;

    public CompactDisc(String title, String category, float cost) {
        super(title,category,cost);
    }

    public String getArtist() {
        return artist;
    }

    public CompactDisc(){
        super();
    }

    public void addTrack(Track track){
        if(this.tracks.contains(track) == true){
            System.out.println("Track already existed");
        }
        else{
            this.tracks.add(track);
        }
    }

    public void removeTrack(Track track){
        if(this.tracks.contains(track) == true){
            this.tracks.remove(track);
        }
        else{
            System.out.println("Track does not exist");
        }
    }

    @Override
    public float getLength(){
        float totalLength = 0;
        for (Track t : tracks){
            totalLength += t.getLength();
        }
        return totalLength;
    }

    public void play(){
        System.out.println("List of Tracks in CD:");
        for (Track t : tracks){
            System.out.println("# " + tracks.indexOf(t) + "Title: " + t.getTitle() + "Length" + t.getLength());
        }
        for (Track t : tracks){
            t.play();
        }
    }

    public int compareTo(Object obj){
        if(obj instanceof CompactDisc == false) return 1;
        CompactDisc o = (CompactDisc) obj;
        int cmp = Integer.compare(this.getId(),o.getId());
        if(cmp != 0)    return cmp;
        cmp = String.CASE_INSENSITIVE_ORDER.compare(this.getTitle(),o.getTitle());
        if(cmp != 0)    return cmp;
        return Integer.compare(this.tracks.size(),o.tracks.size());
    }
}
