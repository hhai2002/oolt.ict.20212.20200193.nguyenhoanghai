package Aims.hust.soict.globalict.aims.media;

public abstract class Media {
    private int id;
    String title;
    String category;
    float cost;

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public float getCost() {
        return cost;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Media){
            return this.id == ((Media)o).id;
        }
        else{
            return false;
        }
    }

    interface CompareTo{

    }
}
