package Aims.hust.soict.globalict.aims.media;

public class Track implements Playable, Comparable{
    private String title;
    private int length;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Track(String title, int length){
        this.setTitle(title);
        this.setLength(length);
    }

    public void play(){
        System.out.println("Playing Track: " + this.getTitle());
        System.out.println("Track length: " + this.getLength());
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Track){
            return this.title.equals(((Track)o).title) & this.length == ((Track) o).length;
        }
        else{
            return false;
        }
    }

    public int compareTo(Object obj){
        if(obj instanceof Track == false)   return 1;
        Track o = (Track) obj;
        int cmp = String.CASE_INSENSITIVE_ORDER.compare(this.getTitle(),o.getTitle());
        if(cmp != 0)    return cmp;
        return Integer.compare(this.length,o.length);
    }
}
