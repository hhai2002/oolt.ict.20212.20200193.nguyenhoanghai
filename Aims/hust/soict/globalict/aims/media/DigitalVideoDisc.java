package Aims.hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable, Comparable{

    private String director;
    private int length;

    public DigitalVideoDisc(String title, String category, float cost, int length) {
        super(title,category,cost);
        this.length = length;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public float getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }



    public DigitalVideoDisc(String title) {
        this.setTitle(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DigitalVideoDisc(String title, String category) {
        this.setTitle(title);
        this.setCategory(category);
    }

    private void setCategory(String category) {
        this.category = category;
    }

    public DigitalVideoDisc(String title, String category, String director) {
        this.setTitle(title);
        this.setCategory(category);
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        this.setTitle(title);
        this.setCategory(category);
        this.director = director;
        this.length = length;
        this.setCost(cost);
    }

    private void setCost(float cost) {
        this.cost = cost;
    }

    public void getDetails(){
        System.out.println("Title: " + this.getTitle());
        System.out.println("Category: " + this.getCategory());
        System.out.println("Director: " + this.getDirector());
        System.out.println("Length: " + this.getLength());
        System.out.println("Cost: " + this.getCost());
    }

    Boolean search(String title){
        return this.getTitle().contains(title);
    }
    public void play(){
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    public int compareTo (Object obj){
        if(obj instanceof DigitalVideoDisc == false)    return 1;
        DigitalVideoDisc o = (DigitalVideoDisc) obj;
        int cmp = Integer.compare(this.getId(),o.getId());
        if(cmp != 0)    return cmp;
        cmp = String.CASE_INSENSITIVE_ORDER.compare(this.getTitle(),o.getTitle());
        if(cmp != 0)    return cmp;
        return Integer.compare(this.length,o.length);

    }
}
