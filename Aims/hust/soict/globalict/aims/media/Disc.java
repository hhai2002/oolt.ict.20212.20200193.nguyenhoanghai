package Aims.hust.soict.globalict.aims.media;

public class Disc extends Media{
    float length;
    String director;

    public Disc(){

    }

    public Disc(float length, String director) {
        super();
        this.length = length;
        this.director = director;
    }

    public Disc(String title, String category, float cost) {
        this.title = title;
        this.category = category;
        this.cost = cost;
    }

    public float getLength() {
        return length;
    }

    public String getDirector() {
        return director;
    }
}
