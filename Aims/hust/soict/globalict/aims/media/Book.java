package Aims.hust.soict.globalict.aims.media;

import java.util.*;

public class Book extends Media implements Comparable{

    private List<String> authors = new ArrayList<String>();

    private String content;

    List<String> contentTokens;

    Map<String,Integer> wordFrequency;
    
    public void processContent(){
        String[] parts = content.split("\\.| |-|,|;|!|\\(|\\)|\\{|}|[|]|\\?");
        for (String p : parts){
            contentTokens.add(p);
        }
        contentTokens.sort(String.CASE_INSENSITIVE_ORDER);
        Iterator<String> s = contentTokens.listIterator();
        while (s.hasNext()){
            String t = s.toString();
            if(wordFrequency.containsKey(t)) s.next();
            wordFrequency.put(t,Collections.frequency(contentTokens,t));
        }
    }

    @Override
    public String toString(){
        String result = "";
        result.concat("ID: " + Integer.toString(this.getId()) + "\n");
        result.concat("Title: " + this.getTitle() + "\n");
        result.concat("Category: " + this.getCategory() + "\n");
        result.concat("Cost: " + Float.toString(this.getCost()) + "\n");
        result.concat("Author: ");
        for (String s: authors){
            result.concat(s + ", ");
        }
        result.concat("Content length: " + Integer.toString(this.contentTokens.size()) + "\n");
        for (String s: wordFrequency.keySet()){
            result.concat(s + " " + wordFrequency.get(s) + "\n");
        }
        return result;
    }

    public Book(String title, String category, float cost) {
        this.title = title;
        this.category = category;
        this.cost = cost;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public Book(){
        // TODO constructor...
    }

    void addAuthor(String authorName){
        if(!this.authors.contains(authorName)){
            this.authors.add(authorName);
        }
    }

    void removeAuthor(String authorName){
        int k = this.authors.size();
        for(int i=0;i<k;++i){
            if(this.authors.get(i).equals(authorName)){
                this.authors.remove(i);
                break;
            }
        }
    }

    public int compareTo(Object obj){
        if(obj instanceof Book == false)    return 1;
        Book o = (Book) obj;
        int cmp = Integer.compare(this.getId(),o.getId());
        if(cmp != 0)    return cmp;
        cmp = String.CASE_INSENSITIVE_ORDER.compare(this.getTitle(),o.getTitle());
        if(cmp != 0)    return cmp;
        return Integer.compare(this.getAuthors().size(),o.getAuthors().size());
    }
}
