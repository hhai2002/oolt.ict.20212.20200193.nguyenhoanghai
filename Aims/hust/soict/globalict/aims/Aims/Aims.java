package Aims.hust.soict.globalict.aims.Aims;

import Aims.hust.soict.globalict.aims.media.Book;
import Aims.hust.soict.globalict.aims.media.CompactDisc;
import Aims.hust.soict.globalict.aims.media.DigitalVideoDisc;
import Aims.hust.soict.globalict.aims.media.Track;
import Aims.hust.soict.globalict.aims.order.Order;
import Aims.hust.soict.globalict.aims.utils.MemoryDaemon;

import java.util.Scanner;

public class Aims {
    public static void main(String[] args) {
        MemoryDaemon mDaemon  = new MemoryDaemon();
        Thread thread = new Thread(mDaemon);
        thread.setDaemon(true);
        thread.start();

        int choice;
        try (Scanner input = new Scanner(System.in)) {
            Order order = null;
            do {
                showMenu();
                choice = input.nextInt(); input.nextLine();
                outer: switch (choice) {
                    case 1:
                        if(Order.getNbOrders() == Order.MAX_LIMITED_ORDERS) {System.out.println("Reach the limitation of number of orders!"); break;}
                        try{
                            order = new Order();
                        }
                        catch (Exception e){

                        }
                        System.out.printf("New order with ID %s has been created.\n", order.getId());
                        break;
                    case 2:
                        if(order != null) {
                            System.out.print("Please enter kind of item: (DVD-Book-CD) ");
                            String kind = input.nextLine();
                            System.out.print("Please enter the title of your " + kind + ": ");
                            String title = input.nextLine();
                            System.out.print("Please enter the category of your " + kind + ": ");
                            String category = input.nextLine();
                            System.out.print("Please enter the cost of your " + kind + ": ");
                            float cost = input.nextFloat(); input.nextLine();

                            switch (kind) {
                                case "DVD":{
                                    System.out.print("Please enter the length (in second) of your DVD: ");
                                    int length = input.nextInt(); input.nextLine();
                                    DigitalVideoDisc item = new DigitalVideoDisc(title, category, cost, length);
                                    item.setId(order.getId()  + order.getNbItems());
                                    order.addMedia(item);
                                    System.out.print("Play DVD?(Y/N) ");
                                    if(input.nextLine().equals("Y")) item.play();
                                    break;
                                }
                                case "CD":{
                                    CompactDisc item = new CompactDisc(title, category, cost);
                                    item.setId(order.getId()  + order.getNbItems());
                                    System.out.print("Add tracks?(Y/N) ");
                                    while(input.nextLine().equals("Y")) {
                                        System.out.print("Enter track's title: "); String trTitle = input.nextLine();
                                        System.out.printf("Enter %s's length (in second): ", trTitle); int trLength = input.nextInt(); input.nextLine();
                                        item.addTrack(new Track(trTitle, trLength));
                                        System.out.print("Continue?(Y/N) ");
                                    }
                                    order.addMedia(item);
                                    System.out.print("Play CD?(Y/N) ");
                                    if(input.nextLine().equals("Y")) item.play();
                                    break;
                                }
                                case "Book":{
                                    Book item = new Book(title, category, cost);
                                    item.setId(order.getId()  + order.getNbItems());
                                    order.addMedia(item);
                                    break;
                                }
                                default:
                                    System.out.println("Invalid kind!");
                                    break outer;
                            }
                        }else System.out.println("No order has been created!");
                        break;
                    case 3:
                        if(order != null) {
                            System.out.print("Please enter item ID: ");
                            int id = input.nextInt();
                            order.removeMedia(id);
                        }else System.out.println("No order has been created!");
                        break;
                    case 4:
                        if(order != null) {
                            order.viewOrder();
                        }else System.out.println("No order has been created!");
                        break;
                    case 0:
                        System.out.println("Exited!");
                        break;
                    default:
                        System.out.println("Invalid option: " + choice);
                        break;
                }
            }while(choice != 0);
        }
    }

    public static void showMenu() {

        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");

    }
}
